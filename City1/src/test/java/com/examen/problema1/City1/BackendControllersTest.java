package com.examen.problema1.City1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
@DisplayName("PRUEBA PARA VACANTE BACKEND JAVA   -  T E S T")
class BackendControllersTest {

	@Autowired
	public MockMvc mvc;

	@Autowired
	ObjectMapper objectMapper;

}
