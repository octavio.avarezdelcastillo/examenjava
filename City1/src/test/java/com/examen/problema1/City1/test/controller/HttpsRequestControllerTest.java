package com.examen.problema1.City1.test.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.MimeTypeUtils;

import com.examen.problema1.City1.BackedControllerTest;

@DisplayName("HTTPSREQUEST - CONTROLLER TEST")
public class HttpsRequestControllerTest extends BackedControllerTest {

	private static final Logger logger = LoggerFactory.getLogger(HttpsRequestControllerTest.class);

	@Nested
	@DisplayName("GET /headers")
	class findAll {

		@Test
		@DisplayName("Should find the all https request headers")
		@Order(1)
		void getHeaders() throws Exception {

			MvcResult mvcResult = (MvcResult) mvc
					.perform(get("/api/headers").accept(MimeTypeUtils.APPLICATION_JSON_VALUE))
					.andExpect(status().isOk()).andReturn();

			logger.info("Response -> {}", mvcResult.getResponse().getContentAsString());

		}

		@Test
		@DisplayName("Should find the filter httpsRequest headers")
		@Order(2)
		void getHeadersFilter() throws Exception {

			List<String> headerList = new ArrayList<String>();

			headerList.add("Accept");
			headerList.add("test");

			logger.info("Request -> {}", objectMapper.writeValueAsString(headerList));

			MvcResult mvcResult = (MvcResult) mvc.perform(get("/api/findheaders")
					.accept(MimeTypeUtils.APPLICATION_JSON_VALUE).content(objectMapper.writeValueAsString(headerList))
					.header("Content-Type", "application/json")).andExpect(status().isOk()).andReturn();

			logger.info("Response -> {}", mvcResult.getResponse().getContentAsString());

		}

	}

	@Nested
	@DisplayName("REMOVE THE ELEMENT REPEAT")
	class filterReapeat {

		@Test
		@DisplayName("GET REMOVE THE INTEGER ")
		void getFilterRepeat() throws Exception {
			MvcResult mvcResult = (MvcResult) mvc.perform(get("/api/repeatnumber")
					.accept(MimeTypeUtils.APPLICATION_JSON_VALUE)).andExpect(status().isOk()).andReturn();

			logger.info("Response -> {}", mvcResult.getResponse().getContentAsString());
		}

	}

}
