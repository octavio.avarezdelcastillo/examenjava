package com.examen.problema1.City1.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.IntFunction;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.examen.problema1.City1.service.HttpsRequestService;

@RestController
@RequestMapping("/api")
public class HttspRequestController {

	Logger logger = LoggerFactory.getLogger(HttspRequestController.class);

	@Autowired
	private HttpsRequestService httpHeaderService;

	@GetMapping("/headers")
	public ResponseEntity<Map<String, List<String>>> getAllHeaders(HttpServletRequest request) throws IOException {

		logger.info("************  Get headers **************");

		Map<String, List<String>> headersResponse = httpHeaderService.getHeaders(request);

		return new ResponseEntity<Map<String, List<String>>>(headersResponse, HttpStatus.OK);
	}

	@GetMapping("/findheaders")
	public Map<String, List<String>> getFindHeader(HttpServletRequest request, @RequestBody List<String> headerNames) {

		logger.info("************  Get find headers **************");

		Map<String, List<String>> headersResponse = httpHeaderService.getHeaders(request);

		return httpHeaderService.getHeadersFilter(headersResponse, headerNames);

	}

	@GetMapping("/repeatnumber")
	public ResponseEntity<List<Integer>> filterRepeatNumber() {
		
		logger.info("************ GET Remove repeats elements **************");
		
		int[] k = { 1, 2, 2, 4, 4, 3, 4, 5, 6 };

		int[] obj = Arrays.stream(k).boxed().collect(Collectors.toList()).stream().distinct()
				.collect(Collectors.toList()).stream().mapToInt(Integer::intValue).toArray();

		return new ResponseEntity<List<Integer>>(Arrays.stream(obj).boxed().collect(Collectors.toList()),
				HttpStatus.OK);

	}

}
