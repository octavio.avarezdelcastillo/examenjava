package com.examen.problema1.City1.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public interface HttpsRequestService {

	public Map<String, List<String>> getHeaders(HttpServletRequest request);
	
	public Map<String, List<String>> getHeadersFilter(Map<String, List<String>> headersResponse, List<String> headerFilter);

}
