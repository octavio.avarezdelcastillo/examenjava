package com.examen.problema1.City1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.examen.problema1.City1.controller,com.examen.problema1.City1.service")
public class City1Application {

	public static void main(String[] args) {
		SpringApplication.run(City1Application.class, args);
	}

}
