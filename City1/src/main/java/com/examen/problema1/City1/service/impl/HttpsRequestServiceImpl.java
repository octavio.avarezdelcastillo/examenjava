package com.examen.problema1.City1.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.examen.problema1.City1.service.HttpsRequestService;

@Service
public class HttpsRequestServiceImpl implements HttpsRequestService {

	@Override
	public Map<String, List<String>> getHeaders(HttpServletRequest request) {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		return Collections.list(httpRequest.getHeaderNames()).stream()
				.collect(Collectors.toMap(Function.identity(), h -> Collections.list(httpRequest.getHeaders(h))));

	}

	@Override
	public Map<String, List<String>> getHeadersFilter(Map<String, List<String>> headersResponse,
			List<String> headerFilter) {

		return headersResponse.entrySet().stream()
				.filter(key -> headerFilter.stream().anyMatch(filter -> filter.equals(key.getKey())))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

	}

}
